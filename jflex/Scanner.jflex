import java_cup.runtime.SymbolFactory;

%%
%cup
%class Scanner
%{
	public Scanner(java.io.InputStream r, SymbolFactory sf){
		this(r);
		this.sf=sf;
	}

    public static String getNumberFromPixel(Object o){
        String[] nums= ((String)o).split("8055");
        String ans = "";
        for(String c : nums){
            if(!c.equals("")){
                ans+=Character.valueOf((char)Integer.parseInt(c.substring(0,2), 16));
            }
        }
        return ans;	}

	private SymbolFactory sf;
%}
%eofval{
    return sf.newSymbol("EOF",sym.EOF);
%eofval}

%%

"000010" { return sf.newSymbol("LPAR", sym.LPAR);}
"000020" { return sf.newSymbol("LCORCH", sym.LCORCH);}
"000030" { return sf.newSymbol("LLLAVE", sym.LLLAVE);}
"000040" { return sf.newSymbol("RPAR", sym.RPAR);}
"000050" { return sf.newSymbol("RCORCH", sym.RCORCH);}
"000060" { return sf.newSymbol("RLLAVE", sym.RLLAVE);}
"000070" { return sf.newSymbol("COMMA", sym.COMMA);}
"000080" { return sf.newSymbol("SEMICOLON", sym.SEMICOLON);}
"0000A0" { return sf.newSymbol("SQUOTE", sym.SQUOTE);}
"0000B0" { return sf.newSymbol("DQUOTE", sym.DQUOTE);}
"0000C0" { return sf.newSymbol("XRETURN", sym.XRETURN);}
"005500" { return sf.newSymbol("EQUALS", sym.EQUALS);}
"005510" { return sf.newSymbol("NOTEQUALS", sym.NOTEQUALS);}
"005520" { return sf.newSymbol("PLUS", sym.PLUS);}
"005530" { return sf.newSymbol("MINUS", sym.MINUS);}
"005540" { return sf.newSymbol("DIVIDE", sym.DIVIDE);}
"005550" { return sf.newSymbol("PROD", sym.PROD);}
"005560" { return sf.newSymbol("POWER", sym.POWER);}
"005570" { return sf.newSymbol("MODULE", sym.MODULE);}
"005580" { return sf.newSymbol("AND", sym.AND);}
"005590" { return sf.newSymbol("OR", sym.OR);}
"0055B0" { return sf.newSymbol("LT", sym.LT);}
"0055C0" { return sf.newSymbol("LE", sym.LE);}
"0055D0" { return sf.newSymbol("GT", sym.GT);}
"00AA20" { return sf.newSymbol("ASSIGNEQUALS", sym.ASSIGNEQUALS);}
"00AA40" { return sf.newSymbol("MINUSEQUALS", sym.MINUSEQUALS);}
"00AA60" { return sf.newSymbol("PLUSEQUALS", sym.PLUSEQUALS);}
"00AA80" { return sf.newSymbol("PRODEQUALS", sym.PRODEQUALS);}
"00AAA0" { return sf.newSymbol("DIVEQUALS", sym.DIVEQUALS);}
"800060" { return sf.newSymbol("XIF", sym.XIF);}
"800070" { return sf.newSymbol("XELSE", sym.XELSE);}
"800080" { return sf.newSymbol("XDO", sym.XDO);}
"8000A0" { return sf.newSymbol("XWHILE", sym.XWHILE);}
"8000B0" { return sf.newSymbol("XREADCHAR", sym.XREADCHAR);}
"8000C0" { return sf.newSymbol("XMAIN", sym.XMAIN);}
"8000D0" { return sf.newSymbol("XTIME", sym.XTIME);}
"8000E0" { return sf.newSymbol("XPRINT", sym.XPRINT);}
"80AA30" { return sf.newSymbol("INT", sym.INT);}
"80AA60" { return sf.newSymbol("BOOLEAN", sym.BOOLEAN);}
"80AA90" { return sf.newSymbol("LONG", sym.LONG);}
"80AAB0" { return sf.newSymbol("CHAR", sym.CHAR);}
"80AAE0" { return sf.newSymbol("STRING", sym.STRING);}


("8055"[0-9a-fA-F][0-9a-fA-F] ([9A-F][0-9A-F][0-9A-F][0-9A-F][0-9A-F][0-9A-F])*)+ { return sf.newSymbol("Integral Number",sym.id, getNumberFromPixel(yytext()));}

([9A-F][0-9A-F][0-9A-F][0-9A-F][0-9A-F][0-9A-F]) { /* ignore white space. */ }
(\n) { /* ignore white space. */ }

. { System.err.println("Illegal character: "+yytext());}