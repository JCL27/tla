/**
 * Created by tritoon on 01/07/16.
 */
public interface AssignedValue {
    Object getValue();

    VarTypes getType();
}
