/**
 * Created by tritoon on 30/06/16.
 */
public class SimpleType {

    private boolean isArray;
    private VarTypes type;

    public SimpleType(int intType, boolean isArray) {
        this.isArray = isArray;
        if (!isArray) {
            switch (intType) {
                case sym.STRING:
                    type = VarTypes.STRING;
                    break;
                case sym.BOOLEAN:
                    type = VarTypes.BOOLEAN;
                    break;
                case sym.CHAR:
                    type = VarTypes.CHAR;
                    break;
                case sym.INT:
                    type = VarTypes.INT;
                    break;
                case sym.LONG:
                    type = VarTypes.LONG;
                    break;
            }
        } else {
            switch (intType) {
                case sym.STRING:
                    type = VarTypes.STRINGA;
                    break;
                case sym.BOOLEAN:
                    type = VarTypes.BOOLEANA;
                    break;
                case sym.CHAR:
                    type = VarTypes.CHARA;
                    break;
                case sym.INT:
                    type = VarTypes.INTA;
                    break;
                case sym.LONG:
                    type = VarTypes.LONGA;
                    break;
            }
        }
    }

    public boolean isArray() {
        return isArray;
    }

    public VarTypes getType() {
        return type;
    }
}
