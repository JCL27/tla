import java.util.HashMap;
import java.util.Map;

public abstract class VarTable {

    private static final String RuntimeNamePrefix = "0RunVar";

    private static Map<String, Var> varMap = new HashMap<String, Var>();

    public static Var addVariable(String varName, Var variable) {
        return varMap.put(varName, variable);
    }

    public static Var addVariable(Var variable) {
        return varMap.put(RuntimeNamePrefix + variable.getId(), variable);
    }

    public static Var getVariable(String varName) {
        return varMap.get(varName);
    }

    public static boolean contains(String varName) {
        return varMap.containsKey(varName);
    }


    public static int getSize() {
        return varMap.size();
    }
}
