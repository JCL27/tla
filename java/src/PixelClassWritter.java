import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class PixelClassWritter {
    private static PixelClassWritter ourInstance = new PixelClassWritter();
    private DataOutputStream dout;
    private static String className = null;

    public static PixelClassWritter getInstance() {
        return ourInstance;
    }

    private PixelClassWritter() {

    }

    public static void setClassName(String className) {
        if (PixelClassWritter.className != null) {
            throw new IllegalStateException("Only can be called once");
        }

        PixelClassWritter.className = className;
        try {
            getInstance().dout = new DataOutputStream(new FileOutputStream(className + ".class"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static String getClassName() {
        return className;
    }

    public void write(byte[] data) {
        try {
            dout.write(data);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void close() {
        try {
            dout.flush();
            dout.close();
      //      System.out.println("Compilation ended");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
