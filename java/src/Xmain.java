import org.objectweb.asm.*;
import org.objectweb.asm.MethodVisitor;

import static org.objectweb.asm.Opcodes.*;

public class Xmain {

    public static void ret() {
        MethodVisitor mv;
        mv = Program.getMethodVisitor();
        mv.visitInsn(RETURN);
        mv.visitMaxs(1, 1);
    }

}
