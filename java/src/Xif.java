import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;

import java.util.Deque;
import java.util.LinkedList;

import static org.objectweb.asm.Opcodes.*;

public class Xif {

    private static Deque<Label> nextElse = new LinkedList<Label>();
    private static Deque<Label> nextEnd = new LinkedList<Label>();


    public static void setCondition(Bool condition) {
        MethodVisitor mv;
        mv = Program.getMethodVisitor();

        mv.visitVarInsn(ILOAD, condition.getVar().getId());

        Label l0 = new Label();
        nextElse.push(l0);
        mv.visitJumpInsn(IFEQ, l0);
    }

    public static void elseStatement() {
        MethodVisitor mv;
        mv = Program.getMethodVisitor();
        Label l1 = new Label();
        nextEnd.push(l1);
        mv.visitJumpInsn(GOTO, l1);
        Label label = nextElse.pop();
        mv.visitLabel(label);
    //  mv.visitFrame(Opcodes.F_APPEND,1, new Object[] {Opcodes.INTEGER}, 0, null);

    }

    public static void endIf() {
    //    System.out.println("endIf");
        MethodVisitor mv;
        mv = Program.getMethodVisitor();

        mv.visitLabel(nextEnd.pop());
    //  mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
    }
}