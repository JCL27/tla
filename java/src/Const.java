public class Const implements AssignedValue {


    private VarTypes type;
    private Object value;

    public Const(VarTypes type, Object value) {
        this.type = type;
        this.value = value;
    }

    public VarTypes getType() {
        return type;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(boolean value) {
        this.value = value;
    }
}
