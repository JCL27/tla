import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

import static org.objectweb.asm.Opcodes.*;

/**
 * Created by tritoon on 30/06/16.
 */
public class Bool implements AssignedValue{
    private Var var = null;
    private Var varL = null;
    private Var varR = null;
    public Bool(String var) {
        if( !VarTable.contains(var)){
            throw new IllegalArgumentException("Variable "+ var +" not initialized");
        }
        this.var = VarTable.getVariable(var);
    }

    public Bool (String lid, int cmp, String rid){

        if(!VarTable.contains(lid) || !VarTable.contains(rid)){
            throw new IllegalArgumentException("Both operands should be declared variables");
        }
        this.varL=VarTable.getVariable(lid);
        this.varR=VarTable.getVariable(rid);
        if(varL.getType()!=VarTypes.BOOLEAN || varR.getType()!=VarTypes.BOOLEAN){
            throw new IllegalArgumentException("Both operans should be booleans "+varL +" "+varR);
        }
        MethodVisitor mv;
        mv = Program.getMethodVisitor();
        var = new Var(VarTypes.BOOLEAN);
        mv.visitVarInsn(ILOAD, varL.getId());
        Label l0 = new Label();
        mv.visitJumpInsn(IFEQ, l0);
        mv.visitVarInsn(ILOAD, varR.getId());
        mv.visitJumpInsn(IFEQ, l0);
        mv.visitInsn(ICONST_1);
        Label l1 = new Label();
        mv.visitJumpInsn(GOTO, l1);
        mv.visitLabel(l0);
        mv.visitFrame(Opcodes.F_APPEND,2, new Object[] {Opcodes.INTEGER, Opcodes.INTEGER}, 0, null);
        mv.visitInsn(ICONST_0);
        mv.visitLabel(l1);
        mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] {Opcodes.INTEGER});
        mv.visitVarInsn(ISTORE, var.getId());

    }



    public Bool(String leftV, int a2, Assval rightV) {
        MethodVisitor mv;
        mv = Program.getMethodVisitor();
        if(!VarTable.contains(leftV)){
           throw new IllegalArgumentException("Left operand must be an initialized variable");
        }
        varL = VarTable.getVariable(leftV);
        if(rightV.getC() instanceof Var) {
            varR = (Var) (rightV.getC());
        }else{
       //     System.out.println("variablezation");
            varR = new Var(rightV.getC().getType(),rightV.getC().getValue());

            if( varR.getType() == VarTypes.STRING) {
                mv.visitLdcInsn(varR.getValue());
                mv.visitVarInsn(ASTORE, varR.getId());
            }else if (varR.getType() == VarTypes.INT){

           //     System.out.println("INT");
                mv.visitLdcInsn(new Integer((Integer)varR.getValue()));
                mv.visitVarInsn(ISTORE, varR.getId());
          //      System.out.println("storing en "+ varR.getId());

            }else if (varR.getType() == VarTypes.LONG) {
                mv.visitLdcInsn(new Long((Long) varR.getValue()));
                mv.visitVarInsn(LSTORE, varR.getId());
            }else if(varR.getType() == VarTypes.CHAR) {
                if((Integer)varR.getValue() <128) {
                    mv.visitIntInsn(BIPUSH, (Integer) varR.getValue());
                }else{
                    mv.visitIntInsn(SIPUSH, (Integer) varR.getValue());
                }
          //      System.out.println("PUSHED: "+((Integer)varR.getValue()));
                mv.visitVarInsn(ISTORE, varR.getId());
            }else if(varR.getType() == VarTypes.BOOLEAN){
                if( (Integer)varR.getValue() == 0){
                    mv.visitInsn(ICONST_0);
                }else{
                    mv.visitInsn(ICONST_1);
                }
                mv.visitVarInsn(ISTORE, varR.getId());
            }


        }
        if (varR.getType() != varL.getType() && varR.getType() != VarTypes.CHAR && varL.getType() != VarTypes.INT) {
            throw new IllegalArgumentException("Cannot compare different types: " + varL.getType().name() + " vs"
                    + varR.getType().name());
        }

        var = new Var(VarTypes.BOOLEAN);
    //    System.out.println("LOSTIPOS SON " + varL.getType().name());
    //    System.out.println(sym.NOTEQUALS + "," + a2);
        switch(a2){
            case sym.EQUALS:

                if(varL.getType()==VarTypes.INT){
                    mv.visitVarInsn(ILOAD, varL.getId());
                    mv.visitVarInsn(ILOAD, varR.getId());
                    Label l0 = new Label();
                    mv.visitJumpInsn(IF_ICMPNE, l0);
                    mv.visitInsn(ICONST_1);
                    Label l1 = new Label();
                    mv.visitJumpInsn(GOTO, l1);
                    mv.visitLabel(l0);
                    mv.visitFrame(Opcodes.F_APPEND,2, new Object[] {Opcodes.INTEGER, Opcodes.INTEGER}, 0, null);
                    mv.visitInsn(ICONST_0);
                    mv.visitLabel(l1);
                    mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] {Opcodes.INTEGER});
                    mv.visitVarInsn(ISTORE, var.getId());
                }else if (varL.getType()==VarTypes.LONG){
                    mv.visitVarInsn(ILOAD, varL.getId());
                    mv.visitVarInsn(ILOAD, varR.getId());
                    Label l0 = new Label();
                    mv.visitJumpInsn(IF_ICMPNE, l0);
                    mv.visitInsn(ICONST_1);
                    Label l1 = new Label();
                    mv.visitJumpInsn(GOTO, l1);
                    mv.visitLabel(l0);
                    mv.visitFrame(Opcodes.F_APPEND,2, new Object[] {Opcodes.INTEGER, Opcodes.INTEGER}, 0, null);
                    mv.visitInsn(ICONST_0);
                    mv.visitLabel(l1);
                    mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] {Opcodes.INTEGER});
                    mv.visitVarInsn(ISTORE, var.getId());
                }else if (varL.getType()==VarTypes.STRING){
                    mv.visitVarInsn(ALOAD, varL.getId());
                    mv.visitVarInsn(ALOAD, varR.getId());
                    Label l0 = new Label();
                    mv.visitJumpInsn(IF_ACMPNE, l0);
                    mv.visitInsn(ICONST_1);
                    Label l1 = new Label();
                    mv.visitJumpInsn(GOTO, l1);
                    mv.visitLabel(l0);
                    mv.visitFrame(Opcodes.F_APPEND,2, new Object[] {"java/lang/String", "java/lang/String"}, 0, null);
                    mv.visitInsn(ICONST_0);
                    mv.visitLabel(l1);
                    mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] {Opcodes.INTEGER});
                    mv.visitVarInsn(ISTORE, var.getId());
                }else if(varL.getType() == VarTypes.CHAR){
                    mv.visitVarInsn(ILOAD, varR.getId());
                    mv.visitVarInsn(ILOAD, varL.getId());
                    Label l0 = new Label();
                    mv.visitJumpInsn(IF_ICMPNE, l0);
                    mv.visitInsn(ICONST_1);
                    Label l1 = new Label();
                    mv.visitJumpInsn(GOTO, l1);
                    mv.visitLabel(l0);
                    mv.visitFrame(Opcodes.F_APPEND,2, new Object[] {Opcodes.INTEGER, Opcodes.INTEGER}, 0, null);
                    mv.visitInsn(ICONST_0);
                    mv.visitLabel(l1);
                    mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] {Opcodes.INTEGER});
                    mv.visitVarInsn(ISTORE, var.getId());
                }else if(varL.getType()==VarTypes.BOOLEAN){
                    mv.visitVarInsn(ILOAD, varL.getId());
                    mv.visitVarInsn(ILOAD, varR.getId());
                    Label l0 = new Label();
                    mv.visitJumpInsn(IF_ICMPNE, l0);
                    mv.visitInsn(ICONST_1);
                    Label l1 = new Label();
                    mv.visitJumpInsn(GOTO, l1);
                    mv.visitLabel(l0);
                    mv.visitFrame(Opcodes.F_APPEND,2, new Object[] {Opcodes.INTEGER, Opcodes.INTEGER}, 0, null);
                    mv.visitInsn(ICONST_0);
                    mv.visitLabel(l1);
                    mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] {Opcodes.INTEGER});
                    mv.visitVarInsn(ISTORE, var.getId());
                }
                break;
            case sym.NOTEQUALS:
          //      System.out.println("NETROACA");

                if(varL.getType()==VarTypes.STRING){
                    mv.visitVarInsn(ALOAD, varL.getId());
                    mv.visitVarInsn(ALOAD, varR.getId());
                    Label l0 = new Label();
                    mv.visitJumpInsn(IF_ACMPEQ, l0);
                    mv.visitInsn(ICONST_1);
                    Label l1 = new Label();
                    mv.visitJumpInsn(GOTO, l1);
                    mv.visitLabel(l0);
                    mv.visitFrame(Opcodes.F_APPEND,2, new Object[] {"java/lang/String", "java/lang/String"}, 0, null);
                    mv.visitInsn(ICONST_0);
                    mv.visitLabel(l1);
                    mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] {Opcodes.INTEGER});
                    mv.visitVarInsn(ISTORE, var.getId());

                }else{
            //        System.out.println("NETROACA");
                    mv.visitVarInsn(ILOAD, varL.getId());
                    mv.visitVarInsn(ILOAD, varR.getId());
                    Label l0 = new Label();
                    mv.visitJumpInsn(IF_ICMPEQ, l0);
                    mv.visitInsn(ICONST_1);
                    Label l1 = new Label();
                    mv.visitJumpInsn(GOTO, l1);
                    mv.visitLabel(l0);
                    //      mv.visitFrame(Opcodes.F_APPEND,2, new Object[] {Opcodes.INTEGER, Opcodes.INTEGER}, 0, null);
                    mv.visitInsn(ICONST_0);
                    mv.visitLabel(l1);
                    //    mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] {Opcodes.INTEGER});
                    mv.visitVarInsn(ISTORE, var.getId());

                }

                break;
            case sym.GT:
                if(varL.getType()==VarTypes.STRING) {
                    throw new IllegalArgumentException("Cannot apply this comparison to strings");
                }else{
                    mv.visitVarInsn(ILOAD, varL.getId());
                    mv.visitVarInsn(ILOAD, varR.getId());
                    Label l0 = new Label();
                    mv.visitJumpInsn(IF_ICMPLE, l0);
                    mv.visitInsn(ICONST_1);
                    Label l1 = new Label();
                    mv.visitJumpInsn(GOTO, l1);
                    mv.visitLabel(l0);
                    mv.visitFrame(Opcodes.F_APPEND,2, new Object[] {Opcodes.INTEGER, Opcodes.INTEGER}, 0, null);
                    mv.visitInsn(ICONST_0);
                    mv.visitLabel(l1);
                    mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] {Opcodes.INTEGER});
                    mv.visitVarInsn(ISTORE, var.getId());
                }
                break;
            case sym.LT:
                if(varL.getType()==VarTypes.STRING) {
                    throw new IllegalArgumentException("Cannot apply this comparison to strings");
                }else{


                    mv.visitInsn(((Integer) varL.getValue()));
                    mv.visitVarInsn(ISTORE, varL.getId());
                    mv.visitInsn(((Integer) varR.getValue()));
                    mv.visitVarInsn(ISTORE, varR.getId());

           //         System.out.println("LT");
                    mv.visitVarInsn(ILOAD, varL.getId());
                    mv.visitVarInsn(ILOAD, varR.getId());
             //       System.out.println("!!"+varL.getValue()+"!"+varR.getValue());
             //       System.out.println("comparando variables "+ varL.getId() + " y " +varR.getId());
                    Label l0 = new Label();
                    mv.visitJumpInsn(IF_ICMPGE, l0);
                    mv.visitInsn(new Integer(1));
                    Label l1 = new Label();
                    mv.visitJumpInsn(GOTO, l1);
                    mv.visitLabel(l0);
             //       mv.visitFrame(Opcodes.F_APPEND,2, new Object[] {Opcodes.INTEGER, Opcodes.INTEGER}, 0, null);
                    mv.visitInsn(new Integer(0));
                    mv.visitLabel(l1);
             //       mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] {Opcodes.INTEGER});

                    mv.visitVarInsn(ISTORE, var.getId());
                }
                break;
            case sym.GE:
                if(varL.getType()==VarTypes.STRING) {
                    throw new IllegalArgumentException("Cannot apply this comparison to strings");
                }else{
                    mv.visitVarInsn(ILOAD, varL.getId());
                    mv.visitVarInsn(ILOAD, varR.getId());
                    Label l0 = new Label();
                    mv.visitJumpInsn(IF_ICMPLT, l0);
                    mv.visitInsn(ICONST_1);
                    Label l1 = new Label();
                    mv.visitJumpInsn(GOTO, l1);
                    mv.visitLabel(l0);
                    mv.visitFrame(Opcodes.F_APPEND,2, new Object[] {Opcodes.INTEGER, Opcodes.INTEGER}, 0, null);
                    mv.visitInsn(ICONST_0);
                    mv.visitLabel(l1);
                    mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] {Opcodes.INTEGER});
                    mv.visitVarInsn(ISTORE, var.getId());
                }
                break;
            case sym.LE:
                    if(varL.getType()== VarTypes.STRING) {
                        throw new IllegalArgumentException("Cannot apply this comparison to strings");
                    }else{
                        mv.visitVarInsn(ILOAD, varL.getId());
                        mv.visitVarInsn(ILOAD, varR.getId());
                        Label l0 = new Label();
                        mv.visitJumpInsn(IF_ICMPGT, l0);
                        mv.visitInsn(ICONST_1);
                        Label l1 = new Label();
                        mv.visitJumpInsn(GOTO, l1);
                        mv.visitLabel(l0);
                        mv.visitFrame(Opcodes.F_APPEND,2, new Object[] {Opcodes.INTEGER, Opcodes.INTEGER}, 0, null);
                        mv.visitInsn(ICONST_0);
                        mv.visitLabel(l1);
                        mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] {Opcodes.INTEGER});
                        mv.visitVarInsn(ISTORE, var.getId());
                    }
                break;
        }
    }

    public Bool(Object a1, Object a2) {

    }
    public Var getVar(){return var;}
    @Override
    public Object getValue() {
        return 1;
    }

    @Override
    public VarTypes getType() {
        return VarTypes.BOOLEAN;
    }
}
