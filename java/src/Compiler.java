import java_cup.runtime.ComplexSymbolFactory;
import java_cup.runtime.Symbol;
import org.objectweb.asm.util.ASMifier;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class Compiler {

    private final static boolean ASMIFIER = false;
    private final static boolean TOKENIZER = false;

    public static void main(String[] args){

        InputStream is = null;
        PixelClassWritter.setClassName("helloWorldHM");
/*
        if(args.length != 1){
            System.out.println("Just one parameter expected: name of source code");
            return;
        }else{
           is = BMPtoPixel.toPixel(args[0]);
        }
        if(is == null){
            System.out.println(String.format("Unable to transform %s into .pixel file.",args[0]));
            return;
        }
*/

        try {
            //is = new FileInputStream("./tests/testIf.hm");
            is = new FileInputStream("./tests/getTime");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        if(ASMIFIER){
            try {
                ASMifier.main(new String[]{"./tests/HelloWorld.class"});
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else {
            if (TOKENIZER) {
                ComplexSymbolFactory csf = new ComplexSymbolFactory();
                Scanner s = new Scanner(is, csf);
                Symbol sym;
                try {
                    while (!(sym = s.next_token()).toString().equals("Symbol: EOF")){
                        String use = sym.toString().split("Symbol\\: ")[1];
                        if (use.equals("SEMICOLON") || use.equals("LLLAVE")) {
                            System.out.println(use);
                        } else {
                            if (use.equals("Integral Number")) {
                               System.out.print(sym.value + " ");
                            } else {
                                System.out.print(use + " ");
                            }
                        }
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                    return;

                }
            } else {

                ComplexSymbolFactory csf = new ComplexSymbolFactory();
                parser p = new parser(new Scanner(is, csf));
                try {
                    System.out.println(p.parse());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }


}
