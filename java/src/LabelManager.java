
import org.objectweb.asm.Label;

import java.util.Stack;

/**
 * Created by juan on 02/07/16.
 */
public abstract class LabelManager {
    private static Stack<LabelPair> labelStack = new Stack<LabelPair>();

    public static void newBlock() {
        labelStack.push(new LabelPair());
    }

    public static Label getStartingLabel() {
        return labelStack.peek().getStart();
    }

    public static Label getEndingLabel() {
        return labelStack.peek().getEnd();
    }

    public static void closeBlock() {
        labelStack.pop();
    }

    static class LabelPair {
        Label start;
        Label end;

        public LabelPair() {
            start = new Label();
            end = new Label();
        }

        public Label getStart() {
            return start;
        }

        public Label getEnd() {
            return end;
        }
    }
}
