import org.objectweb.asm.*;

public class Program {

    public static ClassWriter cw;
    public static MethodVisitor mv;

    public static void startClass() {

        cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES | ClassWriter.COMPUTE_MAXS);

        cw.visit(52, Opcodes.ACC_SUPER, PixelClassWritter.getClassName(), null, "java/lang/Object", null);

        {
            mv = cw.visitMethod(0, "<init>", "()V", null, null);
            mv.visitCode();
            mv.visitVarInsn(Opcodes.ALOAD, 0);
            mv.visitMethodInsn(Opcodes.INVOKESPECIAL, "java/lang/Object", "<init>", "()V", false);
            mv.visitInsn(Opcodes.RETURN);
            mv.visitMaxs(1, 1);
            mv.visitEnd();
        }

        mv = Program.cw.visitMethod(Opcodes.ACC_PUBLIC + Opcodes.ACC_STATIC, "main", "([Ljava/lang/String;)V", null, null);
        mv.visitCode();
    }

    public static void endVisit() {
        mv.visitEnd();
        PixelClassWritter.getInstance().write(cw.toByteArray());
     //   System.out.println("OUT: " + cw.toByteArray());
        cw.visitEnd();
        PixelClassWritter.getInstance().close();
    }

    public Program(Object a1, Object a2) {
    }

    public static MethodVisitor getMethodVisitor() {
        return mv;
    }
}
