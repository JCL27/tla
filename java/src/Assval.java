import org.objectweb.asm.*;
import org.objectweb.asm.MethodVisitor;
import static org.objectweb.asm.Opcodes.*;
/**
 * Created by tritoon on 30/06/16.
 */
public class Assval {

    private AssignedValue c;

    // String or char
    public Assval(String a2, boolean isString) {
        if(isString){
            c = new Const(VarTypes.STRING,a2);
        }else if (a2.length()==1){
            c = new Const(VarTypes.INT, (int) a2.charAt(0));
        //    System.out.println("SOUT : " + a2.charAt(0) + " and " + (int) a2.charAt(0));
        }else{
            throw new IllegalArgumentException("Single char expected");
        }
    }

    // Operation
    public Assval(String a1, Opval operator) {

        MethodVisitor mv;
        mv = Program.getMethodVisitor();

        Assval leftVal = new Assval(a1);

        if (leftVal.getC().getType() != operator.getAssval().getC().getType()) {
            throw new IllegalArgumentException("Cannot operate different types");
        }


        if (leftVal.getC() instanceof Var || operator.getAssval().getC() instanceof Var) {
            Var var1 = null;
            Var var2 = null;

            if (leftVal.getC() instanceof Var) {
                var1 = (Var) leftVal.getC();
            } else {
                var1 = VarTable.addVariable(new Var(leftVal.getC().getType(), leftVal.getC().getValue()));
            }

            if (operator.getAssval().getC() instanceof ArrayElement) {
                Var arrayVar = VarTable.getVariable(a1);
                var2 = VarTable.addVariable(new Var(operator.getAssval().getC().getType()));
                mv.visitVarInsn(Opcodes.ALOAD, arrayVar.getId());
                mv.visitLdcInsn(((ArrayElement) operator.getAssval().getC()).index);
                switch (operator.getAssval().getC().getType()) {
                    case BOOLEAN:
                    case CHAR:
                    case INT:
                        mv.visitInsn(IALOAD);
                        mv.visitVarInsn(ISTORE, var2.getId());
                        break;
                    case LONG:
                        mv.visitInsn(LALOAD);
                        mv.visitVarInsn(LSTORE, var2.getId());
                        break;
                }
            } else if (operator.getAssval().getC() instanceof Var) {
                var2 = (Var) operator.getAssval().getC();
            } else {
                var2 = VarTable.addVariable(new Var(operator.getAssval().getC().getType(), operator.getAssval().getC().getValue()));
            }

            switch (operator.getOperator()) {
                case sym.PLUS:
                    if (leftVal.getC().getType() == VarTypes.STRING) {
                        mv.visitTypeInsn(NEW, "java/lang/StringBuilder");
                        mv.visitInsn(DUP);
                        mv.visitMethodInsn(INVOKESPECIAL, "java/lang/StringBuilder", "<init>", "()V");
                        mv.visitVarInsn(ALOAD, var1.getId());
                        mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;");
                        mv.visitVarInsn(ALOAD, var2.getId());
                        mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;");
                        mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "toString", "()Ljava/lang/String;");
                        this.c = new Var(var1.getType());
                        mv.visitVarInsn(ASTORE, ((Var) this.c).getId());

                    } else {
                        if (leftVal.getC().getType() == VarTypes.LONG) {
                            mv.visitVarInsn(LLOAD, var1.getId());
                            mv.visitVarInsn(LLOAD, var2.getId());
                            mv.visitInsn(LADD);
                            this.c = new Var(var1.getType());
                            mv.visitVarInsn(LSTORE, ((Var) this.c).getId());
                        } else if (leftVal.getC().getType() == VarTypes.INT || leftVal.getC().getType() == VarTypes.CHAR) {
                            mv.visitVarInsn(ILOAD, var1.getId());
                            mv.visitVarInsn(ILOAD, var2.getId());
                            mv.visitInsn(IADD);
                            this.c = new Var(var1.getType());
                            mv.visitVarInsn(ISTORE, ((Var) this.c).getId());
                        } else {
                            throw new IllegalArgumentException("Cannot add booleans");
                        }
                    }
                    break;
                case sym.MINUS:
                    if (leftVal.getC().getType() == VarTypes.LONG) {
                        mv.visitVarInsn(LLOAD, var1.getId());
                        mv.visitVarInsn(LLOAD, var2.getId());
                        mv.visitInsn(LSUB);
                        this.c = new Var(var1.getType());
                        mv.visitVarInsn(LSTORE, ((Var) this.c).getId());
                    } else if (leftVal.getC().getType() == VarTypes.INT || leftVal.getC().getType() == VarTypes.CHAR) {
                        mv.visitVarInsn(ILOAD, var1.getId());
                        mv.visitVarInsn(ILOAD, var2.getId());
                        mv.visitInsn(ISUB);
                        this.c = new Var(var1.getType());
                        mv.visitVarInsn(ISTORE, ((Var) this.c).getId());
                    } else {
                        throw new IllegalArgumentException("Cannot add booleans or strings");
                    }
                    break;
                case sym.DIVIDE:
                    if (leftVal.getC().getType() == VarTypes.LONG) {
                        mv.visitVarInsn(LLOAD, var1.getId());
                        mv.visitVarInsn(LLOAD, var2.getId());
                        mv.visitInsn(LDIV);
                        this.c = new Var(var1.getType());
                        mv.visitVarInsn(LSTORE, ((Var) this.c).getId());
                    } else if (leftVal.getC().getType() == VarTypes.INT || leftVal.getC().getType() == VarTypes.CHAR) {
                        mv.visitVarInsn(ILOAD, var1.getId());
                        mv.visitVarInsn(ILOAD, var2.getId());
                        mv.visitInsn(IDIV);
                        this.c = new Var(var1.getType());
                        mv.visitVarInsn(ISTORE, ((Var) this.c).getId());
                    } else {
                        throw new IllegalArgumentException("Cannot add booleans or strings");
                    }
                    break;
                case sym.PROD:
                    if (leftVal.getC().getType() == VarTypes.LONG) {
                        mv.visitVarInsn(LLOAD, var1.getId());
                        mv.visitVarInsn(LLOAD, var2.getId());
                        mv.visitInsn(LMUL);
                        this.c = new Var(var1.getType());
                        mv.visitVarInsn(LSTORE, ((Var) this.c).getId());
                    } else if (leftVal.getC().getType() == VarTypes.INT || leftVal.getC().getType() == VarTypes.CHAR) {
                        mv.visitVarInsn(ILOAD, var1.getId());
                        mv.visitVarInsn(ILOAD, var2.getId());
                        mv.visitInsn(IMUL);
                        this.c = new Var(var1.getType());
                        mv.visitVarInsn(ISTORE, ((Var) this.c).getId());
                    } else {
                        throw new IllegalArgumentException("Cannot add booleans or strings");
                    }
                    break;
                case sym.MODULE:
                    if (leftVal.getC().getType() == VarTypes.LONG) {
                        mv.visitVarInsn(LLOAD, var1.getId());
                        mv.visitVarInsn(LLOAD, var2.getId());
                        mv.visitInsn(LREM);
                        this.c = new Var(var1.getType());
                        mv.visitVarInsn(LSTORE, ((Var) this.c).getId());
                    } else if (leftVal.getC().getType() == VarTypes.INT || leftVal.getC().getType() == VarTypes.CHAR) {
                        mv.visitVarInsn(ILOAD, var1.getId());
                        mv.visitVarInsn(ILOAD, var2.getId());
                        mv.visitInsn(IREM);
                        this.c = new Var(var1.getType());
                        mv.visitVarInsn(ISTORE, ((Var) this.c).getId());
                    } else {
                        throw new IllegalArgumentException("Cannot add booleans or strings");
                    }
                    break;
                default:
            }


        } else {
            // Both operands are const

            switch (operator.getOperator()) {
                case sym.PLUS:
                    if (leftVal.getC().getType() == VarTypes.STRING) {
                        this.c = new Const(VarTypes.STRING, (String) leftVal.getC().getValue() + operator.getAssval().getC().getValue());
                    } else {
                        if (leftVal.getC().getType() == VarTypes.LONG) {
                            this.c = new Const(leftVal.getC().getType(), (Long) leftVal.getC().getValue() + (Long) operator.getAssval().getC().getValue());
                        } else if (leftVal.getC().getType() != VarTypes.BOOLEAN) {
                            this.c = new Const(leftVal.getC().getType(), (Integer) leftVal.getC().getValue() + (Integer) operator.getAssval().getC().getValue());
                        } else {
                            throw new IllegalArgumentException("Cannot add booleans");
                        }
                    }
                    break;
                case sym.MINUS:
                    if (leftVal.getC().getType() == VarTypes.LONG) {
                        this.c = new Const(leftVal.getC().getType(), (Long) leftVal.getC().getValue() - (Long) operator.getAssval().getC().getValue());
                    } else if (leftVal.getC().getType() != VarTypes.BOOLEAN) {
                        this.c = new Const(leftVal.getC().getType(), (Integer) leftVal.getC().getValue() - (Integer) operator.getAssval().getC().getValue());
                    } else {
                        throw new IllegalArgumentException("Cannot substract booleans");
                    }
                    break;
                case sym.DIVIDE:
                    if (leftVal.getC().getType() == VarTypes.LONG) {
                        this.c = new Const(leftVal.getC().getType(), (Long) leftVal.getC().getValue() / (Long) operator.getAssval().getC().getValue());
                    } else if (leftVal.getC().getType() != VarTypes.BOOLEAN) {
                        this.c = new Const(leftVal.getC().getType(), (Integer) leftVal.getC().getValue() / (Integer) operator.getAssval().getC().getValue());
                    } else {
                        throw new IllegalArgumentException("Cannot divide booleans");
                    }
                    break;
                case sym.PROD:
                    if (leftVal.getC().getType() == VarTypes.LONG) {
                        this.c = new Const(leftVal.getC().getType(), (Long) leftVal.getC().getValue() * (Long) operator.getAssval().getC().getValue());
                    } else if (leftVal.getC().getType() != VarTypes.BOOLEAN) {
                        this.c = new Const(leftVal.getC().getType(), (Integer) leftVal.getC().getValue() * (Integer) operator.getAssval().getC().getValue());
                    } else {
                        throw new IllegalArgumentException("Cannot prod booleans");
                    }
                    break;
                case sym.MODULE:
                    if (leftVal.getC().getType() == VarTypes.LONG) {
                        this.c = new Const(leftVal.getC().getType(), (Long) leftVal.getC().getValue() % (Long) operator.getAssval().getC().getValue());
                    } else if (leftVal.getC().getType() != VarTypes.BOOLEAN) {
                        this.c = new Const(leftVal.getC().getType(), (Integer) leftVal.getC().getValue() % (Integer) operator.getAssval().getC().getValue());
                    } else {
                        throw new IllegalArgumentException("Cannot module (? booleans");
                    }
                    break;
                default:
            }
        }
    }


    public Assval(int a1) {
       // System.out.println(a1);
        if (sym.XTIME == a1) {
        //    System.out.println("XTIME");
            MethodVisitor mv;
            mv = Program.getMethodVisitor();
            Var tmpVar = new Var(VarTypes.INT);
            this.c = tmpVar;
            mv.visitMethodInsn(INVOKESTATIC, "java/lang/System", "currentTimeMillis", "()J");
            mv.visitInsn(L2I);
            mv.visitVarInsn(ISTORE, tmpVar.getId());
        } else if (sym.XREADCHAR == a1) {
       //     System.out.println("XREADCHAR");
            MethodVisitor mv;
            mv = Program.getMethodVisitor();

            Label l0 = new Label();
            Label l1 = new Label();
            Label l2 = new Label();
            mv.visitTryCatchBlock(l0, l1, l2, "java/io/IOException");
            mv.visitInsn(ICONST_0);
            Var tmpVar = new Var(VarTypes.INT);
            this.c = tmpVar;
            mv.visitVarInsn(ISTORE, tmpVar.getId());
            mv.visitLabel(l0);
            mv.visitFieldInsn(GETSTATIC, "java/lang/System", "in", "Ljava/io/InputStream;");
            mv.visitMethodInsn(INVOKEVIRTUAL, "java/io/InputStream", "read", "()I");
            mv.visitVarInsn(ISTORE, tmpVar.getId());
            mv.visitLabel(l1);
            Label l3 = new Label();
            mv.visitJumpInsn(GOTO, l3);
            mv.visitLabel(l2);
            Var catchVar = new Var(VarTypes.INT);
            //   mv.visitFrame(Opcodes.F_FULL, catchVar.getId(), new Object[] {"[Ljava/lang/String;", Opcodes.INTEGER}, tmpVar.getId(), new Object[] {"java/io/IOException"});
            mv.visitVarInsn(ASTORE, catchVar.getId());
            mv.visitVarInsn(ALOAD, catchVar.getId());
            mv.visitMethodInsn(INVOKEVIRTUAL, "java/io/IOException", "printStackTrace", "()V");
            mv.visitLabel(l3);
        }
    }
    //Integer or variable
    public Assval(String a1) {
        if (a1.charAt(0) >= '0' && a1.charAt(0) <= '9') {
            c = new Const(VarTypes.INT, Integer.valueOf(a1));
        } else {
            if (VarTable.contains(a1)) {
                c = VarTable.getVariable(a1);
            } else {
                throw new IllegalArgumentException(" Variable not declared: " + a1);
            }
        }
    }

    public Assval(String lValue, String index) {
    //    System.out.println("new assvall from array: " + lValue + " " + index);

        VarTypes lValueType;
        switch (VarTable.getVariable(lValue).getType()) {
            case BOOLEANA:
                lValueType = VarTypes.BOOLEAN;
                break;
            case CHARA:
                lValueType = VarTypes.CHAR;
                break;
            case INTA:
                lValueType = VarTypes.INT;
                break;
            case STRINGA:
                lValueType = VarTypes.STRING;
                break;
            case LONGA:
                lValueType = VarTypes.LONG;
                break;
            default:
                throw new IllegalArgumentException("Unknown type");
        }

        if (VarTable.contains(lValue)) {
      //      System.out.println("id = " + VarTable.getVariable(lValue).getId());
            c = new ArrayElement(lValueType, index, VarTable.getVariable(lValue).getId());
        } else {
            throw new IllegalArgumentException(" Variable not declared: " + lValue);
        }


    }


    public AssignedValue getC() {
        return c;
    }
}
