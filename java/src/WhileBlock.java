import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

import static org.objectweb.asm.Opcodes.IFEQ;
import static org.objectweb.asm.Opcodes.ILOAD;

/**
 * Created by juan on 02/07/16.
 */
public class WhileBlock {

    static MethodVisitor mv = Program.getMethodVisitor();

    public static void decl(){
        LabelManager.newBlock();
        mv.visitLabel(LabelManager.getStartingLabel());
        // mv.visitFrame(Opcodes.F_APPEND,2, new Object[] {Opcodes.INTEGER, Opcodes.INTEGER}, 0, null);
        //  mv.visitFrame(Opcodes.F_APPEND,1, new Object[] {Opcodes.INTEGER}, 0, null);

        //mv.visitFrame(Opcodes.F_APPEND,3, new Object[] {Opcodes.INTEGER, Opcodes.INTEGER, Opcodes.INTEGER}, 0, null);
        // mv.visitFrame(Opcodes.F_APPEND,1, new Object[] {Opcodes.INTEGER, OpCodes.AXES_END_TYPES}, 0, null);//Magic line -,.-~*´¨¯¨`*·~-.¸
    }

    public static void start(Bool condition){
        mv.visitVarInsn(ILOAD, condition.getVar().getId());
        mv.visitJumpInsn(IFEQ, LabelManager.getEndingLabel());
        //   mv.visitVarInsn(ILOAD, condition.getVar().getId());
        //   mv.visitLdcInsn(new Integer(1));
        //  mv.visitInsn(Opcodes.IF_ICMPNE);
        //  mv.visitJumpInsn(Opcodes.IF_ICMPNE, LabelManager.getEndingLabel());

    }

    public static void end(){
        mv.visitJumpInsn(Opcodes.GOTO, LabelManager.getStartingLabel());
        mv.visitLabel(LabelManager.getEndingLabel());
        LabelManager.closeBlock();
        //    mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);// Another magic line -,.-~*´¨¯¨`*·~-.¸
    }
}
