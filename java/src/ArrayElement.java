/**
 * Created by juan on 02/07/16.
 */
public class ArrayElement extends Var {
    String index;

    public ArrayElement(VarTypes type, String index, int id) {
        super(type, id);
        this.index = index;
    }

    public String getIndex() {
        return index;
    }

}
