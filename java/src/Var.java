
public class Var implements AssignedValue {


    private static int cont = 1;

    private VarTypes type;
    private Object value;
    private int id;

    public Var(VarTypes type, Object value) {
        this.type = type;
        this.value = value;
        this.id = cont;
        cont++;
    }

    public Var(VarTypes type) {
        this.type = type;
        this.value = null;
        this.id = cont;
        cont++;
    }

    public Var(VarTypes type, int id) {
        this.type = type;
        this.value = null;
        this.id = id;
    }

    public VarTypes getType() {
        return type;
    }

    public Object getValue() {
        return value;
    }

    public int getId() {
        return id;
    }

    public static int getTotalVariables() {
        return cont - 1;
    }
}
