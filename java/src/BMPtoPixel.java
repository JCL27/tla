import java.io.*;

public class BMPtoPixel {

    private static final String OUT_EXTENSION = ".pixel";

    public static InputStream toPixel(String file) {

        FileInputStream in = null;
        FileOutputStream out = null;
        String pixelFile = null;
        try {
            in = new FileInputStream(file);
            pixelFile = "tmp"+OUT_EXTENSION;
            out = new FileOutputStream(pixelFile);

            bmpParse(in,out);

            in.close();
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            return new FileInputStream(pixelFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }


    private static void bmpParse(FileInputStream in, FileOutputStream out) throws IOException {
        int width = 0, height = 0, cont=0, lines = 0, hex = 0, totalBytes = 0, c, discard = 0;
        int bgr[] = new int[3];
        int bytesWritten=0;
        boolean writting = true;
        String skylines[] = new String[0];

        while ((c = in.read()) != -1) {
            if(cont >= 18 && cont <= 21){
                width += (int) Math.pow(256,cont-18)*c;
            }
            else if(cont >= 22 && cont <= 25){
                height += (int) Math.pow(256,cont-22)*c;
                if(cont==25) {
                    skylines = new String[height];
                }
            } else if (cont >= 34 && cont <= 37){
                totalBytes += (int) Math.pow(256,cont-34)*c;
                if(cont == 37){
                    discard = Math.min(0,(totalBytes-(width*height*3))/height);
                }
            } else if (cont >= 54) {
                bytesWritten++;
                if (writting) {
                    bgr[hex] = c;

                    if (hex++ == 2) {
                        if(skylines[lines] != null) {
                            skylines[lines] += String.format("%02X%02X%02X", bgr[2], bgr[1], bgr[0]);
                        }else{
                            skylines[lines] = String.format("%02X%02X%02X", bgr[2], bgr[1], bgr[0]);

                        }
                        hex = 0;
                    }

                    if (bytesWritten == width * 3) {
                        writting = false;
                        bytesWritten = 0;
                        lines++;
                    }
                    if(discard==0){
                        writting = true;
                    }
                } else {
                    if (bytesWritten == discard) {
                        writting = true;
                        bytesWritten = 0;
                    }
                }
            }

            cont++;
        }

        for(int i = 0; i<lines;i++){
            out.write(skylines[i].getBytes());
        }

        //  System.out.println(width+" , "+height);
        //  System.out.println("Lines: "+lines);
    }

}