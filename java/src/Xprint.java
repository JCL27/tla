import org.objectweb.asm.*;
import org.objectweb.asm.MethodVisitor;

import static org.objectweb.asm.Opcodes.*;

public class Xprint {
    public static void invoke(String a1) {
        int toPrint = 0;
        int printType = 0;
        String param = null;
        if (a1.charAt(0) >= '0' && a1.charAt(0) <= '9') {
            throw new IllegalArgumentException("Cannot print integer, only variables allowed");
        } else {
            if (VarTable.contains(a1)) {
                Var leftVal = VarTable.getVariable(a1);
                toPrint = leftVal.getId();
          //      System.out.println("TOPRINT ES " + toPrint);
                if (leftVal.getType() == VarTypes.LONG) {
                    printType = LLOAD;
                    param = "(J)V";
                } else if (leftVal.getType() == VarTypes.CHAR) {
                    printType = ILOAD;
                    param = "(C)V";
                } else if (leftVal.getType() == VarTypes.INT) {
                    printType = ILOAD;
                    param = "(I)V";
                } else if (leftVal.getType() == VarTypes.BOOLEAN) {
                    printType = ILOAD;
                    param = "(Z)V";
                } else if (leftVal.getType() == VarTypes.STRING) {
                    printType = ALOAD;
                    param = "(Ljava/lang/String;)V";
            //        System.out.println("param es " + param);
                }

            } else {
                throw new IllegalArgumentException(" Variable not declared: " + a1);
            }
        }
        MethodVisitor mv;
        mv = Program.getMethodVisitor();

        mv.visitFieldInsn(GETSTATIC, "java/lang/System", "out", "Ljava/io/PrintStream;");
        mv.visitVarInsn(printType, toPrint);
        mv.visitMethodInsn(INVOKEVIRTUAL, "java/io/PrintStream", "println", param);
    }


}
