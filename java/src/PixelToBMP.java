import java.io.*;

public class PixelToBMP {


    public static void main(String args[]){

        String file = "./tests/tateti/tateti.pixel";

        FileInputStream in;
        FileOutputStream out;
        String pixelFile = "out.bmp";


        try {
            File sourceFile = new File(file);
            in = new FileInputStream(sourceFile);
            out = new FileOutputStream(pixelFile);

            pixelParse(sourceFile.length(),in,out);

            in.close();
            out.close();
            //    System.out.println("Output at: "+pixelFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    private static void pixelParse(long sourceLength, FileInputStream in, FileOutputStream out) throws IOException {


        String header1 = "424d66000000000000003600000028000000";
        String header2 ="000000";
        String header3 ="0000000100180000000000";
        String header4 ="000000000000000000000000000000000000";

        int side = ((int)Math.sqrt(sourceLength))+1;
        String sside = Integer.toHexString(side);
        if(sside.length()==1){
            sside+="0";
        }

        String tsize = Integer.toHexString(side*side);

        while(tsize.length()<4){
            tsize+="0";
        }

        for(int i=0;i<header1.length();i+=2)
            out.write((int) Long.parseLong(""+header1.charAt(i)+header1.charAt(i+1), 16));
        for(int i=0;i<sside.length();i+=2)
            out.write((int) Long.parseLong(""+sside.charAt(i)+sside.charAt(i+1), 16));
        for(int i=0;i<header2.length();i+=2)
            out.write((int) Long.parseLong(""+header2.charAt(i)+header2.charAt(i+1), 16));
        for(int i=0;i<sside.length();i+=2)
            out.write((int) Long.parseLong(""+sside.charAt(i)+sside.charAt(i+1), 16));
        for(int i=0;i<header3.length();i+=2)
            out.write((int) Long.parseLong(""+header3.charAt(i)+header3.charAt(i+1), 16));
        for(int i=0;i<tsize.length();i+=2)
            out.write((int) Long.parseLong(""+tsize.charAt(i)+tsize.charAt(i+1), 16));
        for(int i=0;i<header4.length();i+=2)
            out.write((int) Long.parseLong(""+header4.charAt(i)+header4.charAt(i+1), 16));

        int bytesWritte = 0;


        String[] rgb = new String[3];
        int r, pixels = 0;
        while (((r = in.read()) != -1) ){
            String pixel = ""+Character.valueOf((char) r);
            while(pixel.length()<2){
                pixel += Character.valueOf((char) in.read());
            }
            rgb[0]=pixel;

            pixel = "";
            while(pixel.length()<2){
                pixel += Character.valueOf((char) in.read());
            }
            rgb[1]=pixel;


            pixel = "";
            while(pixel.length()<2){
                pixel += Character.valueOf((char) in.read());
            }
            rgb[2]=pixel;

            out.write((int) Long.parseLong(""+rgb[2], 16));
            out.write((int) Long.parseLong(""+rgb[1], 16));
            out.write((int) Long.parseLong(""+rgb[0], 16));
            bytesWritte+= 1;
        }

        //   System.out.println("Written: "+bytesWritte);
        while(bytesWritte<side*side){
            out.write((int) Long.parseLong("EE", 16));
            bytesWritte+=1;
        }

        //    System.out.println("File of "+sourceLength+" bytes into side: "+side+" (0x"+sside+")");

    }

}