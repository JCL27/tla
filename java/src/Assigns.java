import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

import static org.objectweb.asm.Opcodes.*;

/**
 * Created by tritoon on 01/07/16.
 */
public class Assigns {
    public Assigns(String lValue, Asym asym) {
        MethodVisitor mv = Program.getMethodVisitor();
        if (!VarTable.contains(lValue)) {
            throw new IllegalArgumentException(lValue + " is not declared!");
        } else if (VarTable.getVariable(lValue).getType() != asym.getAssval().getC().getType()) {
            throw new IllegalArgumentException("Types do not match.");
        }
        VarTypes lValueType = VarTable.getVariable(lValue).getType();
        int lValueId = VarTable.getVariable(lValue).getId();
        AssignedValue rValue = asym.getAssval().getC();
        switch (asym.getSgn()) {
            case sym.PLUSEQUALS:
                if (lValueType == VarTypes.STRING) {
                    Var auxVar;
                    if (rValue instanceof Const) {
                        auxVar = VarTable.addVariable(new Var(rValue.getType(), rValue.getValue()));
                    } else {
                        auxVar = (Var) rValue;
                    }
                    mv.visitTypeInsn(NEW, "java/lang/StringBuilder");
                    mv.visitInsn(DUP);
                    mv.visitMethodInsn(INVOKESPECIAL, "java/lang/StringBuilder", "<init>", "()V");
                    mv.visitVarInsn(ALOAD, lValueId);
                    mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;");
                    mv.visitVarInsn(ALOAD, auxVar.getId());
                    mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;");
                    mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "toString", "()Ljava/lang/String;");
                    mv.visitVarInsn(ASTORE, lValueId);
                } else if (lValueType == VarTypes.INT || lValueType == VarTypes.CHAR) {
                    mv.visitVarInsn(Opcodes.ILOAD, lValueId);
                    if (rValue instanceof Var) {
                        Var var = (Var) rValue;
                        mv.visitVarInsn(Opcodes.ILOAD, var.getId());
                    } else {
                        Const constant = (Const) rValue;
                        mv.visitLdcInsn(Integer.valueOf((int) constant.getValue()));
                    }
                    mv.visitInsn(Opcodes.IADD);
                    mv.visitVarInsn(Opcodes.ISTORE, lValueId);

                } else if (lValueType == VarTypes.LONG) {
                    mv.visitVarInsn(Opcodes.LLOAD, lValueId);
                    if (rValue instanceof Var) {
                        Var var = (Var) rValue;
                        mv.visitVarInsn(Opcodes.LLOAD, var.getId());
                    } else {
                        Const constant = (Const) rValue;
                        mv.visitLdcInsn(Long.valueOf((Long) constant.getValue()));
                    }
                    mv.visitInsn(Opcodes.LADD);
                    mv.visitVarInsn(Opcodes.LSTORE, lValueId);
                } else {
                    throw new IllegalArgumentException("Invalid r value");
                }
                break;
            case sym.ASSIGNEQUALS:
                if (lValueType == VarTypes.STRING) {
                    Var auxVar;
                    if (rValue instanceof Const) {
                        auxVar = VarTable.addVariable(new Var(rValue.getType(), rValue.getValue()));
                    } else {
                        auxVar = (Var) rValue;
                    }
                    mv.visitVarInsn(ALOAD, auxVar.getId());
                    mv.visitVarInsn(ASTORE, lValueId);
                } else if (lValueType == VarTypes.INT || lValueType == VarTypes.CHAR) {
               //     System.out.println("lValue is int");

                    if (rValue instanceof ArrayElement) {
          //              System.out.println("id " + ((ArrayElement) rValue).getId() + " index " + ((ArrayElement) rValue).getIndex());
                        Var var = (Var) rValue;
                        mv.visitVarInsn(Opcodes.ALOAD, var.getId());
                        String index = ((ArrayElement) rValue).getIndex();
                        try {
                            int intIndex = Integer.parseInt(index);
                            mv.visitLdcInsn(intIndex);
                        } catch (NumberFormatException e) {
                            if(VarTable.contains(index)) {
                                mv.visitVarInsn(ILOAD, VarTable.getVariable(index).getId());
                            }else {
                                throw new IllegalArgumentException("Invalid array index");
                            }
                        }
                        mv.visitInsn(Opcodes.IALOAD);
                    } else if (rValue instanceof Var) {
                        Var var = (Var) rValue;
                        mv.visitVarInsn(Opcodes.ILOAD, var.getId());
                    } else {
                        Const constant = (Const) rValue;
                        mv.visitLdcInsn(Integer.valueOf((Integer) constant.getValue()));
                    }
                    mv.visitVarInsn(Opcodes.ISTORE, lValueId);

                } else if (lValueType == VarTypes.LONG) {
                    if (rValue instanceof Var) {
                        Var var = (Var) rValue;
                        mv.visitVarInsn(Opcodes.LLOAD, var.getId());
                    } else {
                        Const constant = (Const) rValue;
                        mv.visitLdcInsn(Long.valueOf((Long) constant.getValue()));
                    }
                    mv.visitVarInsn(Opcodes.LSTORE, lValueId);
                } else if (lValueType == VarTypes.BOOLEAN) {
                    if (rValue instanceof Var) {
                        Var var = (Var) rValue;
                        mv.visitVarInsn(Opcodes.ILOAD, var.getId());
                    } else {
                        Const constant = (Const) rValue;
                        mv.visitLdcInsn(Integer.valueOf((Integer) constant.getValue()));
                    }
                    mv.visitVarInsn(Opcodes.ISTORE, lValueId);
                } else {
                    throw new IllegalArgumentException("Invalid r value");
                }
                break;
            case sym.MINUSEQUALS:
                if (lValueType == VarTypes.INT || lValueType == VarTypes.CHAR) {
                    mv.visitVarInsn(Opcodes.ILOAD, lValueId);
                    if (rValue instanceof Var) {
                        Var var = (Var) rValue;
                        mv.visitVarInsn(Opcodes.ILOAD, var.getId());
                    } else {
                        Const constant = (Const) rValue;
                        mv.visitLdcInsn(Integer.valueOf((Integer) constant.getValue()));
                    }
                    mv.visitInsn(Opcodes.ISUB);
                    mv.visitVarInsn(Opcodes.ISTORE, lValueId);

                } else if (lValueType == VarTypes.LONG) {
                    mv.visitVarInsn(Opcodes.LLOAD, lValueId);
                    if (rValue instanceof Var) {
                        Var var = (Var) rValue;
                        mv.visitVarInsn(Opcodes.LLOAD, var.getId());
                    } else {
                        Const constant = (Const) rValue;
                        mv.visitLdcInsn(Long.valueOf((Long) constant.getValue()));
                    }
                    mv.visitInsn(Opcodes.LSUB);
                    mv.visitVarInsn(Opcodes.LSTORE, lValueId);
                } else {
                    throw new IllegalArgumentException("Invalid r value");
                }
                break;
            case sym.PRODEQUALS:
                if (lValueType == VarTypes.INT || lValueType == VarTypes.CHAR) {
                    mv.visitVarInsn(Opcodes.ILOAD, lValueId);
                    if (rValue instanceof Var) {
                        Var var = (Var) rValue;
                        mv.visitVarInsn(Opcodes.ILOAD, var.getId());
                    } else {
                        Const constant = (Const) rValue;
                        mv.visitLdcInsn(Integer.valueOf((Integer) constant.getValue()));
                    }
                    mv.visitInsn(Opcodes.IMUL);
                    mv.visitVarInsn(Opcodes.ISTORE, lValueId);

                } else if (lValueType == VarTypes.LONG) {
                    mv.visitVarInsn(Opcodes.LLOAD, lValueId);
                    if (rValue instanceof Var) {
                        Var var = (Var) rValue;
                        mv.visitVarInsn(Opcodes.LLOAD, var.getId());
                    } else {
                        Const constant = (Const) rValue;
                        mv.visitLdcInsn(Long.valueOf((Long) constant.getValue()));
                    }
                    mv.visitInsn(Opcodes.LMUL);
                    mv.visitVarInsn(Opcodes.LSTORE, lValueId);
                } else {
                    throw new IllegalArgumentException("Invalid r value");
                }
                break;
            case sym.DIVEQUALS:
                if (lValueType == VarTypes.INT || lValueType == VarTypes.CHAR) {
                    mv.visitVarInsn(Opcodes.ILOAD, lValueId);
                    if (rValue instanceof Var) {
                        Var var = (Var) rValue;
                        mv.visitVarInsn(Opcodes.ILOAD, var.getId());
                    } else {
                        Const constant = (Const) rValue;
                        mv.visitLdcInsn(Integer.valueOf((Integer) constant.getValue()));
                    }
                    mv.visitInsn(Opcodes.IDIV);
                    mv.visitVarInsn(Opcodes.ISTORE, lValueId);

                } else if (lValueType == VarTypes.LONG) {
                    mv.visitVarInsn(Opcodes.LLOAD, lValueId);
                    if (rValue instanceof Var) {
                        Var var = (Var) rValue;
                        mv.visitVarInsn(Opcodes.LLOAD, var.getId());
                    } else {
                        Const constant = (Const) rValue;
                        mv.visitLdcInsn(Long.valueOf((Long) constant.getValue()));
                    }
                    mv.visitInsn(Opcodes.LDIV);
                    mv.visitVarInsn(Opcodes.LSTORE, lValueId);
                } else {
                    throw new IllegalArgumentException("Invalid r value");
                }
                break;
        }
    }

    public Assigns(String lValue, String index, Assval assval) {
        MethodVisitor mv = Program.getMethodVisitor();
        if (!VarTable.contains(lValue)) {
            throw new IllegalArgumentException(lValue + " is not declared!");
        }

        VarTypes lValueType;
        switch (VarTable.getVariable(lValue).getType()) {
            case BOOLEANA:
                lValueType = VarTypes.BOOLEAN;
                break;
            case CHARA:
                lValueType = VarTypes.CHAR;
                break;
            case INTA:
                lValueType = VarTypes.INT;
                break;
            case STRINGA:
                lValueType = VarTypes.STRING;
                break;
            case LONGA:
                lValueType = VarTypes.LONG;
                break;
            default:
                throw new IllegalArgumentException("Unknown type");
        }

        if (lValueType != assval.getC().getType()) {
            throw new IllegalArgumentException("Types do not match.");
        }

        int lValueId = VarTable.getVariable(lValue).getId();

   //     System.out.println("type = " + lValueType);

        AssignedValue rValue = assval.getC();

        mv.visitVarInsn(Opcodes.ALOAD, lValueId);
        try {
            int intIndex = Integer.parseInt(index);
            mv.visitLdcInsn(intIndex);
        } catch (NumberFormatException e) {
            if(VarTable.contains(index)) {
                mv.visitVarInsn(ILOAD, VarTable.getVariable(index).getId());
            }else {
                throw new IllegalArgumentException("Invalid array index");
            }
        }
        if (lValueType == VarTypes.STRING) {
            Var auxVar;
            if (rValue instanceof Const) {
                auxVar = VarTable.addVariable(new Var(rValue.getType(), rValue.getValue()));
            } else {
                auxVar = (Var) rValue;
            }
            mv.visitVarInsn(ALOAD, auxVar.getId());
            mv.visitInsn(AASTORE);

        } else if (lValueType == VarTypes.INT || lValueType == VarTypes.CHAR) {
            if (rValue instanceof Var) {
                Var var = (Var) rValue;
                mv.visitVarInsn(Opcodes.ILOAD, var.getId());
            } else {
                Const constant = (Const) rValue;
                mv.visitLdcInsn(Integer.valueOf((int) constant.getValue()));
            }
            mv.visitInsn(IASTORE);

        } else if (lValueType == VarTypes.LONG) {
            if (rValue instanceof Var) {
                Var var = (Var) rValue;
                mv.visitVarInsn(Opcodes.LLOAD, var.getId());
            } else {
                Const constant = (Const) rValue;
                mv.visitLdcInsn(Long.valueOf((long) constant.getValue()));
            }
            mv.visitInsn(LASTORE);
        } else if (lValueType == VarTypes.BOOLEAN) {
            if (rValue instanceof Var) {
                Var var = (Var) rValue;
                mv.visitVarInsn(Opcodes.ILOAD, var.getId());
            } else {
                Const constant = (Const) rValue;
                mv.visitLdcInsn(Integer.valueOf((int) constant.getValue()));
            }
            mv.visitInsn(IASTORE);

        } else {
            throw new IllegalArgumentException("Invalid r value");
        }

    }


}
