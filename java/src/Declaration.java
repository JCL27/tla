import org.objectweb.asm.*;
import org.objectweb.asm.Opcodes;

/**
 * Created by tritoon on 30/06/16.
 */
public class Declaration {


    public static void addVar(SimpleType st, String var, Assval asym) {
        MethodVisitor mv = Program.getMethodVisitor();

        if (!st.isArray()) {

            if (st.getType() == VarTypes.BOOLEAN && asym.getC() instanceof Const) {
                if ((int) (asym.getC()).getValue() != 1 && (int) (asym.getC()).getValue() == 0) {
                    throw new IllegalArgumentException("Booleans should be 1 for TRUE or 0 for FALSE");
                }
            } else if (st.getType() != asym.getC().getType()) {
                throw new IllegalArgumentException("Types dont match");
            } else if (VarTable.contains(var)) {
                throw new IllegalArgumentException("Variable " + var + " already exists");
            }


            for (int i = 0; i < var.length(); i++) {
                if (i == 0 && !(var.charAt(i) >= 'A' && var.charAt(i) <= 'Z') && !(var.charAt(i) >= 'a' && var.charAt(i) <= 'z')) {
                    throw new IllegalArgumentException("Variable name should begin with a letter: " + var);
                } else if (!(var.charAt(i) >= 'A' && var.charAt(i) <= 'Z') && !(var.charAt(i) >= 'a' && var.charAt(i) <= 'z')
                        && !(var.charAt(i) >= '0' && var.charAt(i) <= '9')) {

                }
            }


            Var newVar = new Var(st.getType(), asym.getC().getValue());
        //    System.out.println(st.getType() + "," + var + " = " + asym.getC().getValue() + " : " + newVar.getId());

            VarTable.addVariable(var, newVar);


            if (asym.getC() instanceof Const) {
                if (st.getType() == VarTypes.STRING) {
                    mv.visitLdcInsn(asym.getC().getValue());
                    mv.visitVarInsn(Opcodes.ASTORE, newVar.getId());
                } else if (st.getType() == VarTypes.INT) {
                    mv.visitLdcInsn(new Integer((int) asym.getC().getValue()));
                    mv.visitVarInsn(Opcodes.ISTORE, newVar.getId());
                } else if (st.getType() == VarTypes.LONG) {
                    mv.visitLdcInsn(new Long((long) asym.getC().getValue()));
                    mv.visitVarInsn(Opcodes.LSTORE, newVar.getId());
                } else if (st.getType() == VarTypes.CHAR) {
                    if ((int) asym.getC().getValue() < 128) {
                        mv.visitIntInsn(Opcodes.BIPUSH, (int) asym.getC().getValue());
                    } else {
                        mv.visitIntInsn(Opcodes.SIPUSH, (int) asym.getC().getValue());
                    }
            //        System.out.println("PUSHED: " + ((int) asym.getC().getValue()));
                    mv.visitVarInsn(Opcodes.ISTORE, newVar.getId());
                } else if (st.getType() == VarTypes.BOOLEAN) {
                    if ((int) asym.getC().getValue() == 0) {
                        mv.visitInsn(Opcodes.ICONST_0);
                    } else {
                        mv.visitInsn(Opcodes.ICONST_1);
                    }
                    mv.visitVarInsn(Opcodes.ISTORE, (int) (asym.getC().getValue()));
                }
            } else {
                Var rightVar = ((Var) asym.getC());
                if (st.getType() == VarTypes.STRING) {
                    mv.visitVarInsn(Opcodes.ALOAD, rightVar.getId());
                    mv.visitVarInsn(Opcodes.ASTORE, newVar.getId());
                } else if (st.getType() == VarTypes.INT) {
                    mv.visitVarInsn(Opcodes.ILOAD, rightVar.getId());
                    mv.visitVarInsn(Opcodes.ISTORE, newVar.getId());
                } else if (st.getType() == VarTypes.LONG) {
                    mv.visitVarInsn(Opcodes.LLOAD, rightVar.getId());
                    mv.visitVarInsn(Opcodes.LSTORE, newVar.getId());
                } else if (st.getType() == VarTypes.CHAR) {
                    mv.visitVarInsn(Opcodes.ILOAD, rightVar.getId());
                    mv.visitVarInsn(Opcodes.ISTORE, newVar.getId());
                } else if (st.getType() == VarTypes.BOOLEAN) {
                    mv.visitVarInsn(Opcodes.ILOAD, rightVar.getId());
                    mv.visitVarInsn(Opcodes.ISTORE, (int) asym.getC().getValue());
                }
            }
        } else {
            mv.visitIntInsn(Opcodes.SIPUSH, (int) asym.getC().getValue());
            int type;
            switch (st.getType()) {
                case LONGA:
                    type = Opcodes.T_LONG;
                    break;
                case INTA:
                    type = Opcodes.T_INT;
                    break;
                case CHARA:
                    type = Opcodes.T_CHAR;
                    break;
                case BOOLEANA:
                    type = Opcodes.T_BOOLEAN;
                    break;
                default:
                    throw new IllegalArgumentException("Type not allowed in an array");
            }
            mv.visitIntInsn(Opcodes.NEWARRAY, type);

            Var newVar = new Var(st.getType());
            VarTable.addVariable(var, newVar);

            mv.visitVarInsn(Opcodes.ASTORE, newVar.getId());

        }

        mv.visitEnd();


    }
}
