/* The following code was generated by JFlex 1.4.1 on 7/3/16 2:07 PM */

import java_cup.runtime.SymbolFactory;


/**
 * This class is a scanner generated by 
 * <a href="http://www.jflex.de/">JFlex</a> 1.4.1
 * on 7/3/16 2:07 PM from the specification file
 * <tt>./jflex/Scanner.jflex</tt>
 */
class Scanner implements java_cup.runtime.Scanner {

  /** This character denotes the end of file */
  public static final int YYEOF = -1;

  /** initial size of the lookahead buffer */
  private static final int ZZ_BUFFERSIZE = 16384;

  /** lexical states */
  public static final int YYINITIAL = 0;

  /** 
   * Translates characters to character classes
   */
  private static final char [] ZZ_CMAP = {
     0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 18,  0,  0,  0,  0,  0, 
     0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 
     0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 
     1,  2,  3,  4,  5,  6,  7,  8,  9, 13,  0,  0,  0,  0,  0,  0, 
     0, 10, 11, 12, 14, 15, 17,  0,  0,  0,  0,  0,  0,  0,  0,  0, 
     0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 
     0, 16, 16, 16, 16, 16, 16,  0,  0,  0,  0,  0,  0,  0,  0,  0, 
     0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0
  };

  /** 
   * Translates DFA states to action switch labels.
   */
  private static final int [] ZZ_ACTION = zzUnpackAction();

  private static final String ZZ_ACTION_PACKED_0 =
    "\1\0\4\1\1\2\75\0\1\3\1\4\1\5\1\6"+
    "\1\7\1\10\1\11\1\12\1\13\1\14\1\15\1\16"+
    "\1\17\1\20\1\21\1\22\1\23\1\24\1\25\1\26"+
    "\1\27\1\30\1\31\1\32\1\33\1\34\1\35\1\36"+
    "\1\37\1\40\1\41\1\42\1\43\1\44\1\45\1\46"+
    "\1\47\1\50\1\51\1\52\1\53\1\54\1\55\7\0";

  private static int [] zzUnpackAction() {
    int [] result = new int[117];
    int offset = 0;
    offset = zzUnpackAction(ZZ_ACTION_PACKED_0, offset, result);
    return result;
  }

  private static int zzUnpackAction(String packed, int offset, int [] result) {
    int i = 0;       /* index in packed string  */
    int j = offset;  /* index in unpacked array */
    int l = packed.length();
    while (i < l) {
      int count = packed.charAt(i++);
      int value = packed.charAt(i++);
      do result[j++] = value; while (--count > 0);
    }
    return j;
  }


  /** 
   * Translates a state to a row index in the transition table
   */
  private static final int [] ZZ_ROWMAP = zzUnpackRowMap();

  private static final String ZZ_ROWMAP_PACKED_0 =
    "\0\0\0\23\0\46\0\71\0\114\0\23\0\137\0\162"+
    "\0\205\0\230\0\253\0\276\0\321\0\344\0\367\0\u010a"+
    "\0\u011d\0\u0130\0\u0143\0\u0156\0\u0169\0\u017c\0\u018f\0\u01a2"+
    "\0\u01b5\0\u01c8\0\u01db\0\u01ee\0\u0201\0\u0214\0\u0227\0\u023a"+
    "\0\u024d\0\u0260\0\u0273\0\u0286\0\u0299\0\u02ac\0\u02bf\0\u02d2"+
    "\0\u02e5\0\u02f8\0\u030b\0\u031e\0\u0331\0\u0344\0\u0357\0\u036a"+
    "\0\u037d\0\u0390\0\u03a3\0\u03b6\0\u03c9\0\u03dc\0\u03ef\0\u0402"+
    "\0\u0415\0\u0428\0\u043b\0\u044e\0\u0461\0\u0474\0\u0487\0\u049a"+
    "\0\u04ad\0\u04c0\0\u04d3\0\23\0\23\0\23\0\23\0\23"+
    "\0\23\0\23\0\23\0\23\0\23\0\23\0\23\0\23"+
    "\0\23\0\23\0\23\0\23\0\23\0\23\0\23\0\23"+
    "\0\23\0\23\0\23\0\23\0\23\0\23\0\23\0\23"+
    "\0\23\0\23\0\23\0\23\0\23\0\23\0\23\0\23"+
    "\0\u04e6\0\23\0\23\0\23\0\23\0\23\0\u04f9\0\u050c"+
    "\0\u051f\0\u0532\0\u0545\0\u0558\0\u056b";

  private static int [] zzUnpackRowMap() {
    int [] result = new int[117];
    int offset = 0;
    offset = zzUnpackRowMap(ZZ_ROWMAP_PACKED_0, offset, result);
    return result;
  }

  private static int zzUnpackRowMap(String packed, int offset, int [] result) {
    int i = 0;  /* index in packed string  */
    int j = offset;  /* index in unpacked array */
    int l = packed.length();
    while (i < l) {
      int high = packed.charAt(i++) << 16;
      result[j++] = high | packed.charAt(i++);
    }
    return j;
  }

  /** 
   * The transition table of the DFA
   */
  private static final int [] ZZ_TRANS = zzUnpackTrans();

  private static final String ZZ_TRANS_PACKED_0 =
    "\1\2\1\3\7\2\1\4\6\5\1\2\1\5\1\6"+
    "\24\0\1\7\22\0\1\10\22\0\17\11\1\0\1\11"+
    "\2\0\1\12\4\0\1\13\3\0\1\14\11\0\1\15"+
    "\4\0\1\16\3\0\1\17\11\0\17\20\1\0\1\20"+
    "\2\0\1\21\27\0\1\22\26\0\1\23\11\0\1\24"+
    "\27\0\1\25\26\0\1\26\11\0\17\27\1\0\1\27"+
    "\3\0\1\30\1\31\1\32\1\33\1\34\1\35\1\36"+
    "\1\37\1\40\1\41\1\42\7\0\1\43\1\44\1\45"+
    "\1\46\1\47\1\50\1\51\1\52\1\53\1\0\1\54"+
    "\1\55\1\56\1\57\7\0\1\60\1\0\1\61\1\0"+
    "\1\62\1\0\1\63\1\64\17\0\1\65\1\66\1\67"+
    "\1\70\1\71\1\72\1\0\1\73\1\74\4\0\21\75"+
    "\5\0\1\76\2\0\1\77\3\0\1\100\1\0\1\101"+
    "\1\0\1\102\4\0\17\103\1\0\1\103\2\0\1\104"+
    "\22\0\1\105\22\0\1\106\22\0\1\107\22\0\1\110"+
    "\22\0\1\111\22\0\1\112\22\0\1\113\22\0\1\114"+
    "\22\0\1\115\22\0\1\116\22\0\1\117\22\0\1\120"+
    "\22\0\1\121\22\0\1\122\22\0\1\123\22\0\1\124"+
    "\22\0\1\125\22\0\1\126\22\0\1\127\22\0\1\130"+
    "\22\0\1\131\22\0\1\132\22\0\1\133\22\0\1\134"+
    "\22\0\1\135\22\0\1\136\22\0\1\137\22\0\1\140"+
    "\22\0\1\141\22\0\1\142\22\0\1\143\22\0\1\144"+
    "\22\0\1\145\22\0\1\146\22\0\1\147\22\0\1\150"+
    "\22\0\21\151\2\0\1\152\22\0\1\153\22\0\1\154"+
    "\22\0\1\155\22\0\1\156\22\0\17\6\1\0\1\6"+
    "\12\0\1\157\6\160\1\0\1\160\2\0\1\161\22\0"+
    "\17\162\1\0\1\162\7\0\1\16\15\0\17\163\1\0"+
    "\1\163\2\0\17\164\1\0\1\164\2\0\17\165\1\0"+
    "\1\165\2\0\17\151\1\0\1\151\1\0";

  private static int [] zzUnpackTrans() {
    int [] result = new int[1406];
    int offset = 0;
    offset = zzUnpackTrans(ZZ_TRANS_PACKED_0, offset, result);
    return result;
  }

  private static int zzUnpackTrans(String packed, int offset, int [] result) {
    int i = 0;       /* index in packed string  */
    int j = offset;  /* index in unpacked array */
    int l = packed.length();
    while (i < l) {
      int count = packed.charAt(i++);
      int value = packed.charAt(i++);
      value--;
      do result[j++] = value; while (--count > 0);
    }
    return j;
  }


  /* error codes */
  private static final int ZZ_UNKNOWN_ERROR = 0;
  private static final int ZZ_NO_MATCH = 1;
  private static final int ZZ_PUSHBACK_2BIG = 2;

  /* error messages for the codes above */
  private static final String ZZ_ERROR_MSG[] = {
    "Unkown internal scanner error",
    "Error: could not match input",
    "Error: pushback value was too large"
  };

  /**
   * ZZ_ATTRIBUTE[aState] contains the attributes of state <code>aState</code>
   */
  private static final int [] ZZ_ATTRIBUTE = zzUnpackAttribute();

  private static final String ZZ_ATTRIBUTE_PACKED_0 =
    "\1\0\1\11\3\1\1\11\75\0\45\11\1\1\5\11"+
    "\7\0";

  private static int [] zzUnpackAttribute() {
    int [] result = new int[117];
    int offset = 0;
    offset = zzUnpackAttribute(ZZ_ATTRIBUTE_PACKED_0, offset, result);
    return result;
  }

  private static int zzUnpackAttribute(String packed, int offset, int [] result) {
    int i = 0;       /* index in packed string  */
    int j = offset;  /* index in unpacked array */
    int l = packed.length();
    while (i < l) {
      int count = packed.charAt(i++);
      int value = packed.charAt(i++);
      do result[j++] = value; while (--count > 0);
    }
    return j;
  }

  /** the input device */
  private java.io.Reader zzReader;

  /** the current state of the DFA */
  private int zzState;

  /** the current lexical state */
  private int zzLexicalState = YYINITIAL;

  /** this buffer contains the current text to be matched and is
      the source of the yytext() string */
  private char zzBuffer[] = new char[ZZ_BUFFERSIZE];

  /** the textposition at the last accepting state */
  private int zzMarkedPos;

  /** the textposition at the last state to be included in yytext */
  private int zzPushbackPos;

  /** the current text position in the buffer */
  private int zzCurrentPos;

  /** startRead marks the beginning of the yytext() string in the buffer */
  private int zzStartRead;

  /** endRead marks the last character in the buffer, that has been read
      from input */
  private int zzEndRead;

  /** number of newlines encountered up to the start of the matched text */
  private int yyline;

  /** the number of characters up to the start of the matched text */
  private int yychar;

  /**
   * the number of characters from the last newline up to the start of the 
   * matched text
   */
  private int yycolumn;

  /** 
   * zzAtBOL == true <=> the scanner is currently at the beginning of a line
   */
  private boolean zzAtBOL = true;

  /** zzAtEOF == true <=> the scanner is at the EOF */
  private boolean zzAtEOF;

  /** denotes if the user-EOF-code has already been executed */
  private boolean zzEOFDone;

  /* user code: */
	public Scanner(java.io.InputStream r, SymbolFactory sf){
		this(r);
		this.sf=sf;
	}

    public static String getNumberFromPixel(Object o){
        String[] nums= ((String)o).split("8055");
        String ans = "";
        for(String c : nums){
            if(!c.equals("")){
                ans+=Character.valueOf((char)Integer.parseInt(c.substring(0,2), 16));
            }
        }
        return ans;	}

	private SymbolFactory sf;


  /**
   * Creates a new scanner
   * There is also a java.io.InputStream version of this constructor.
   *
   * @param   in  the java.io.Reader to read input from.
   */
  Scanner(java.io.Reader in) {
    this.zzReader = in;
  }

  /**
   * Creates a new scanner.
   * There is also java.io.Reader version of this constructor.
   *
   * @param   in  the java.io.Inputstream to read input from.
   */
  Scanner(java.io.InputStream in) {
    this(new java.io.InputStreamReader(in));
  }


  /**
   * Refills the input buffer.
   *
   * @return      <code>false</code>, iff there was new input.
   * 
   * @exception   java.io.IOException  if any I/O-Error occurs
   */
  private boolean zzRefill() throws java.io.IOException {

    /* first: make room (if you can) */
    if (zzStartRead > 0) {
      System.arraycopy(zzBuffer, zzStartRead,
                       zzBuffer, 0,
                       zzEndRead-zzStartRead);

      /* translate stored positions */
      zzEndRead-= zzStartRead;
      zzCurrentPos-= zzStartRead;
      zzMarkedPos-= zzStartRead;
      zzPushbackPos-= zzStartRead;
      zzStartRead = 0;
    }

    /* is the buffer big enough? */
    if (zzCurrentPos >= zzBuffer.length) {
      /* if not: blow it up */
      char newBuffer[] = new char[zzCurrentPos*2];
      System.arraycopy(zzBuffer, 0, newBuffer, 0, zzBuffer.length);
      zzBuffer = newBuffer;
    }

    /* finally: fill the buffer with new input */
    int numRead = zzReader.read(zzBuffer, zzEndRead,
                                            zzBuffer.length-zzEndRead);

    if (numRead < 0) {
      return true;
    }
    else {
      zzEndRead+= numRead;
      return false;
    }
  }

    
  /**
   * Closes the input stream.
   */
  public final void yyclose() throws java.io.IOException {
    zzAtEOF = true;            /* indicate end of file */
    zzEndRead = zzStartRead;  /* invalidate buffer    */

    if (zzReader != null)
      zzReader.close();
  }


  /**
   * Resets the scanner to read from a new input stream.
   * Does not close the old reader.
   *
   * All internal variables are reset, the old input stream 
   * <b>cannot</b> be reused (internal buffer is discarded and lost).
   * Lexical state is set to <tt>ZZ_INITIAL</tt>.
   *
   * @param reader   the new input stream 
   */
  public final void yyreset(java.io.Reader reader) {
    zzReader = reader;
    zzAtBOL  = true;
    zzAtEOF  = false;
    zzEndRead = zzStartRead = 0;
    zzCurrentPos = zzMarkedPos = zzPushbackPos = 0;
    yyline = yychar = yycolumn = 0;
    zzLexicalState = YYINITIAL;
  }


  /**
   * Returns the current lexical state.
   */
  public final int yystate() {
    return zzLexicalState;
  }


  /**
   * Enters a new lexical state
   *
   * @param newState the new lexical state
   */
  public final void yybegin(int newState) {
    zzLexicalState = newState;
  }


  /**
   * Returns the text matched by the current regular expression.
   */
  public final String yytext() {
    return new String( zzBuffer, zzStartRead, zzMarkedPos-zzStartRead );
  }


  /**
   * Returns the character at position <tt>pos</tt> from the 
   * matched text. 
   * 
   * It is equivalent to yytext().charAt(pos), but faster
   *
   * @param pos the position of the character to fetch. 
   *            A value from 0 to yylength()-1.
   *
   * @return the character at position pos
   */
  public final char yycharat(int pos) {
    return zzBuffer[zzStartRead+pos];
  }


  /**
   * Returns the length of the matched text region.
   */
  public final int yylength() {
    return zzMarkedPos-zzStartRead;
  }


  /**
   * Reports an error that occured while scanning.
   *
   * In a wellformed scanner (no or only correct usage of 
   * yypushback(int) and a match-all fallback rule) this method 
   * will only be called with things that "Can't Possibly Happen".
   * If this method is called, something is seriously wrong
   * (e.g. a JFlex bug producing a faulty scanner etc.).
   *
   * Usual syntax/scanner level error handling should be done
   * in error fallback rules.
   *
   * @param   errorCode  the code of the errormessage to display
   */
  private void zzScanError(int errorCode) {
    String message;
    try {
      message = ZZ_ERROR_MSG[errorCode];
    }
    catch (ArrayIndexOutOfBoundsException e) {
      message = ZZ_ERROR_MSG[ZZ_UNKNOWN_ERROR];
    }

    throw new Error(message);
  } 


  /**
   * Pushes the specified amount of characters back into the input stream.
   *
   * They will be read again by then next call of the scanning method
   *
   * @param number  the number of characters to be read again.
   *                This number must not be greater than yylength()!
   */
  public void yypushback(int number)  {
    if ( number > yylength() )
      zzScanError(ZZ_PUSHBACK_2BIG);

    zzMarkedPos -= number;
  }


  /**
   * Contains user EOF-code, which will be executed exactly once,
   * when the end of file is reached
   */
  private void zzDoEOF() throws java.io.IOException {
    if (!zzEOFDone) {
      zzEOFDone = true;
      yyclose();
    }
  }


  /**
   * Resumes scanning until the next regular expression is matched,
   * the end of input is encountered or an I/O-Error occurs.
   *
   * @return      the next token
   * @exception   java.io.IOException  if any I/O-Error occurs
   */
  public java_cup.runtime.Symbol next_token() throws java.io.IOException {
    int zzInput;
    int zzAction;

    // cached fields:
    int zzCurrentPosL;
    int zzMarkedPosL;
    int zzEndReadL = zzEndRead;
    char [] zzBufferL = zzBuffer;
    char [] zzCMapL = ZZ_CMAP;

    int [] zzTransL = ZZ_TRANS;
    int [] zzRowMapL = ZZ_ROWMAP;
    int [] zzAttrL = ZZ_ATTRIBUTE;

    while (true) {
      zzMarkedPosL = zzMarkedPos;

      zzAction = -1;

      zzCurrentPosL = zzCurrentPos = zzStartRead = zzMarkedPosL;
  
      zzState = zzLexicalState;


      zzForAction: {
        while (true) {
    
          if (zzCurrentPosL < zzEndReadL)
            zzInput = zzBufferL[zzCurrentPosL++];
          else if (zzAtEOF) {
            zzInput = YYEOF;
            break zzForAction;
          }
          else {
            // store back cached positions
            zzCurrentPos  = zzCurrentPosL;
            zzMarkedPos   = zzMarkedPosL;
            boolean eof = zzRefill();
            // get translated positions and possibly new buffer
            zzCurrentPosL  = zzCurrentPos;
            zzMarkedPosL   = zzMarkedPos;
            zzBufferL      = zzBuffer;
            zzEndReadL     = zzEndRead;
            if (eof) {
              zzInput = YYEOF;
              break zzForAction;
            }
            else {
              zzInput = zzBufferL[zzCurrentPosL++];
            }
          }
          int zzNext = zzTransL[ zzRowMapL[zzState] + zzCMapL[zzInput] ];
          if (zzNext == -1) break zzForAction;
          zzState = zzNext;

          int zzAttributes = zzAttrL[zzState];
          if ( (zzAttributes & 1) == 1 ) {
            zzAction = zzState;
            zzMarkedPosL = zzCurrentPosL;
            if ( (zzAttributes & 8) == 8 ) break zzForAction;
          }

        }
      }

      // store back cached position
      zzMarkedPos = zzMarkedPosL;

      switch (zzAction < 0 ? zzAction : ZZ_ACTION[zzAction]) {
        case 36: 
          { return sf.newSymbol("XREADCHAR", sym.XREADCHAR);
          }
        case 46: break;
        case 45: 
          { return sf.newSymbol("STRING", sym.STRING);
          }
        case 47: break;
        case 34: 
          { return sf.newSymbol("XDO", sym.XDO);
          }
        case 48: break;
        case 15: 
          { return sf.newSymbol("NOTEQUALS", sym.NOTEQUALS);
          }
        case 49: break;
        case 16: 
          { return sf.newSymbol("PLUS", sym.PLUS);
          }
        case 50: break;
        case 13: 
          { return sf.newSymbol("XRETURN", sym.XRETURN);
          }
        case 51: break;
        case 7: 
          { return sf.newSymbol("RCORCH", sym.RCORCH);
          }
        case 52: break;
        case 42: 
          { return sf.newSymbol("BOOLEAN", sym.BOOLEAN);
          }
        case 53: break;
        case 11: 
          { return sf.newSymbol("SQUOTE", sym.SQUOTE);
          }
        case 54: break;
        case 17: 
          { return sf.newSymbol("MINUS", sym.MINUS);
          }
        case 55: break;
        case 23: 
          { return sf.newSymbol("LT", sym.LT);
          }
        case 56: break;
        case 22: 
          { return sf.newSymbol("AND", sym.AND);
          }
        case 57: break;
        case 2: 
          { /* ignore white space. */
          }
        case 58: break;
        case 43: 
          { return sf.newSymbol("CHAR", sym.CHAR);
          }
        case 59: break;
        case 14: 
          { return sf.newSymbol("EQUALS", sym.EQUALS);
          }
        case 60: break;
        case 41: 
          { return sf.newSymbol("INT", sym.INT);
          }
        case 61: break;
        case 8: 
          { return sf.newSymbol("RLLAVE", sym.RLLAVE);
          }
        case 62: break;
        case 40: 
          { return sf.newSymbol("Integral Number",sym.id, getNumberFromPixel(yytext()));
          }
        case 63: break;
        case 6: 
          { return sf.newSymbol("RPAR", sym.RPAR);
          }
        case 64: break;
        case 4: 
          { return sf.newSymbol("LCORCH", sym.LCORCH);
          }
        case 65: break;
        case 44: 
          { return sf.newSymbol("LONG", sym.LONG);
          }
        case 66: break;
        case 21: 
          { return sf.newSymbol("MODULE", sym.MODULE);
          }
        case 67: break;
        case 32: 
          { return sf.newSymbol("XIF", sym.XIF);
          }
        case 68: break;
        case 18: 
          { return sf.newSymbol("DIVIDE", sym.DIVIDE);
          }
        case 69: break;
        case 3: 
          { return sf.newSymbol("LPAR", sym.LPAR);
          }
        case 70: break;
        case 27: 
          { return sf.newSymbol("ASSIGNEQUALS", sym.ASSIGNEQUALS);
          }
        case 71: break;
        case 30: 
          { return sf.newSymbol("PRODEQUALS", sym.PRODEQUALS);
          }
        case 72: break;
        case 38: 
          { return sf.newSymbol("XTIME", sym.XTIME);
          }
        case 73: break;
        case 5: 
          { return sf.newSymbol("LLLAVE", sym.LLLAVE);
          }
        case 74: break;
        case 29: 
          { return sf.newSymbol("PLUSEQUALS", sym.PLUSEQUALS);
          }
        case 75: break;
        case 39: 
          { return sf.newSymbol("XPRINT", sym.XPRINT);
          }
        case 76: break;
        case 26: 
          { return sf.newSymbol("GT", sym.GT);
          }
        case 77: break;
        case 25: 
          { return sf.newSymbol("OR", sym.OR);
          }
        case 78: break;
        case 28: 
          { return sf.newSymbol("MINUSEQUALS", sym.MINUSEQUALS);
          }
        case 79: break;
        case 9: 
          { return sf.newSymbol("COMMA", sym.COMMA);
          }
        case 80: break;
        case 12: 
          { return sf.newSymbol("DQUOTE", sym.DQUOTE);
          }
        case 81: break;
        case 37: 
          { return sf.newSymbol("XMAIN", sym.XMAIN);
          }
        case 82: break;
        case 20: 
          { return sf.newSymbol("POWER", sym.POWER);
          }
        case 83: break;
        case 1: 
          { System.err.println("Illegal character: "+yytext());
          }
        case 84: break;
        case 19: 
          { return sf.newSymbol("PROD", sym.PROD);
          }
        case 85: break;
        case 24: 
          { return sf.newSymbol("LE", sym.LE);
          }
        case 86: break;
        case 35: 
          { return sf.newSymbol("XWHILE", sym.XWHILE);
          }
        case 87: break;
        case 31: 
          { return sf.newSymbol("DIVEQUALS", sym.DIVEQUALS);
          }
        case 88: break;
        case 33: 
          { return sf.newSymbol("XELSE", sym.XELSE);
          }
        case 89: break;
        case 10: 
          { return sf.newSymbol("SEMICOLON", sym.SEMICOLON);
          }
        case 90: break;
        default: 
          if (zzInput == YYEOF && zzStartRead == zzCurrentPos) {
            zzAtEOF = true;
            zzDoEOF();
              {     return sf.newSymbol("EOF",sym.EOF);
 }
          } 
          else {
            zzScanError(ZZ_NO_MATCH);
          }
      }
    }
  }


}
