/**
 * Created by tritoon on 30/06/16.
 */
public class Asym {
    private int sgn;
    private Assval assval;

    public Asym(int sgn, Assval a1) {
        this.sgn = sgn;
        this.assval = a1;
    }

    public int getSgn() {
        return sgn;
    }

    public Assval getAssval() {
        return assval;
    }
}
