/**
 * Created by tritoon on 30/06/16.
 */
public class Opval {

    private int operator;
    private Assval assval;

    public Opval(int operator, Assval assval) {
        this.operator = operator;
        this.assval = assval;
    }

    public int getOperator() {
        return operator;
    }

    public Assval getAssval() {
        return assval;
    }
}
