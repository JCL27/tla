#!/bin/bash

java -jar ./bin/java-cup-11a.jar ./cup/Parser.cup
java -jar ./bin/JFlex.jar ./jflex/Scanner.jflex
mv ./jflex/Scanner.java ./java/src/Scanner.java
mv ./parser.java ./java/src/parser.java
mv ./sym.java ./java/src/sym.java
javac -cp ./bin/java-cup-11a.jar:./lib/asm-5.1.jar:./lib/asm-util-5.1.jar ./java/src/Scanner.java ./java/src/sym.java ./java/src/parser.java ./java/src/*.java -d ./classes
